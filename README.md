# HOTEL-MANAGEMENT-PROJECT-IN-C

## About

A Hotel Management System simulation that simulates a multi-day business in a hotel. See <code>Description</code> for more details.

## Installation

Download and Compile .c file and run exe file

## Screenshots

<table>
  <tbody>
    <tr>
      <td align="center">
          <img width="390" alt="LOGIN" src="/screenshorts/login.png">
          <br>
      </td>
      <td align="center">
          <img width="390" alt="MENU" src="/screenshorts/MAIN_MENU.png">
          <br>
      </td>
    </tr>   
    <tr>
      <td align="center">
          <img width="390" alt="AVAILABILITY" src="/screenshorts/ROOM_AVAILABILITY.png">
          <br>
      </td>
      <td align="center">
          <img width="390" alt="BOOK_ROOM" src="/screenshorts/BOOK_ROOM.png">
          <br>
      </td>
    </tr>   
    <tr>
      <td align="center">
          <img width="390" alt="SEARCH" src="/screenshorts/SEARCH.png">
          <br>
      </td>
      <td align="center">
          <img width="390" alt="VIEW" src="/screenshorts/VIEW_RECORD.png">
          <br>
      </td>
    </tr>   
      <tr>
      <td align="center" colspan="2">
          <img width="780" alt="BILL" src="/screenshorts/BILLING.png">
          <br>
      </td>
    </tr>   
  </tbody>
</table>

## Description

The major feature of this program is:

1. Login the administrator through username and password if both are valid then gone to main menu.

2. See all vacant room in hotel.

3. Book Any room which is present in hotel and if room is vacant and enter all correct information of the customer.

4. Search record of Particular room  .

5. Edit Customer record.

6. Bill generation.

7. Delete record once the bill is generated.

